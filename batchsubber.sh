#!/bin/bash

outputdir="/sc/orga/scratch/belmoj01/"

# List the contents of the directory passed as argument 1 and read all files there into an array 
infiles=($(ls -p ${1} | grep -v /))

cd ${1}

# Loop through the array and ship off each command to bsub
for i in "${infiles[@]}"
do
  Command="ml perl5;perl -I /sc/orga/projects/pintod02b/hadjie_wd/LncTar_working/LncTar/LncTar/ /sc/orga/projects/pintod02b/hadjie_wd/LncTar_working/LncTar/LncTar/LncTar.pl -p 1 -l /sc/orga/projects/pintod02b/hadjie_wd/LncTar_working/mature_homo.fa.txt -m $i -d -0.13 -s F -o ${outputdir}$i"
  echo ${Command}
  bsub -J noncoding_aa -P acc_pintod02b -q premium -n 20 -W 20:00 -R rusage[mem=3000] -o /sc/orga/scratch/belmoj01/lsf-output/$i ${Command}
done
