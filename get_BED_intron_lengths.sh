# Reads a 12-column BED file and spits out a '\n'-delimited file listing the lengths of each intron
bedFile=$1
outFile=$2

awk '{nex=split($11,bsizearr,","); split($12,bstarr,","); for (i=2;i<nex;i++){if (bsizearr[i] != "") print (bstarr[i]-(bstarr[i-1]+bsizearr[i-1]))}}' $1 > $2