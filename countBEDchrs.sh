countme=$1

chrs=($(seq 1 1 22))
chrs+=('Y')
chrs+=('X')

for i in "${chrs[@]}"
do
  echo -ne "chr$i "
  awk '{print $1}' ${countme} | grep -wc chr$i
done