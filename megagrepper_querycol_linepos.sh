#!/bin/bash

echo $1 # list of query terms, 1 per line
echo $2 # file to grep the query terms on
echo $3 # output file to write the grep hits to

# read query terms into array
while read qterm; do queries+=($qterm); done < $1

if [ -f $3 ]; then
   rm $3
fi

# Approach A: iterate over query term array and grep each term one at a time & append results
for i in "${queries[@]}"
do
   grep -F "$i" $2 | awk -v query=$i 'BEGIN { FS = OFS = "\t"; }; {start=index($0,query); end=start+(length(query)-1); print query,start,end,$0}' >> $3
done
# Approach B: grep everything together & write results to new file
#grep ${queries[@]/#/-Fwe } $2 > $3
