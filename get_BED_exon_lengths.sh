# Reads a 12-column BED file and spits out a '\n'-delimited file listing the lengths of each exon
bedFile=$1
outFile=$2

awk '{split($11,arr,",")} { for (i in arr) if(arr[i] != "") print arr[i] }' $1 > $2